%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Script to convert the TMM model output to netcdf of matlab files          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%
% Edit the following two lines to specify the path to the directory your model code is in,
% then the directory name within this containing the outout from the run you want to process repectively
% Once edited and saved, this script can be run by pasting the following into the command line:
% matlab -nodisplay
% run load_output_with_restarts.m
% exit
%%%%%

input_directory_base = '/gpfs/ts0/home/ph290/TMM/tmm/Work_ocmipabioticcarbon_with_alk5/';
job_id = 'job_2018.11.08_20.15.40_979730';

% if you do not have acess to the undergraduate_Teaching-148395 project space, you will need to point to your own copies of these directories
addpath(genpath('/gpfs/ts0/projects/Undergraduate_Teaching-148395/tmm_matlab_code'))
base_path='/gpfs/ts0/projects/Undergraduate_Teaching-148395/tmm/MITgcm_2.8deg';

%%%%%
% You should not need to edit any of the code below here
%%%%%

input_directory = strcat(input_directory_base,job_id,'/');
short_job_id = strsplit(job_id,'b');
short_job_id = char(short_job_id(2));


%addpath(genpath('/gpfs/ts0/home/ph290/tmm/tmm_matlab_code'))
%base_path='/data2/spk/TransportMatrixConfigs/MITgcm_2.8deg';
%base_path='/gpfs/ts0/home/ph290/tmm/MITgcm_2.8deg';

load(fullfile(base_path,'config_data'))

matrixPath=fullfile(base_path,matrixPath);

gridFile=fullfile(base_path,'grid');
boxFile=fullfile(matrixPath,'Data','boxes');
profilesFile=fullfile(matrixPath,'Data','profile_data');

load(gridFile,'nx','ny','nz','x','y','z','gridType')

load(profilesFile,'Irr')

trNames={'dic','dic14','alk'};

numTr=length(trNames);

if strcmp(gridType,'llc_v4')
  load(fullfile(base_path,'llc_v4_grid'))
  gcmfaces_global
end

filePattern = fullfile(input_directory, [trNames{1} short_job_id '.petsc*']);
files = dir(filePattern);
no_files = length(files);
file_appends=[];
for itr=1:no_files;
	filename = files(itr);
	file_appends=[file_appends; filename.name(end-3:end)];
end

for itr=1:numTr
  varName=upper(trNames{itr})
  for itr2=1:no_files
      itr2
	  fn=[input_directory trNames{itr} short_job_id '.petsc' file_appends(itr2,:)];
	  tr=readPetscBinVec(fn,1,-1);
	  TR=matrixToGrid(tr(Irr,end),[],boxFile,gridFile);

	  if strcmp(gridType,'llc_v4')
		varName=[varName '_plot'];
		tmp=gcmfaces(TR);
		[x,y,TRplot]=convert2pcol(mygrid.XC,mygrid.YC,tmp);
		[n1,n2]=size(TRplot);
		eval([varName '=zeros([n1 n2 nz]);']);
		for iz=1:nz
		  eval(['[x,y,' varName '(:,:,iz)]=convert2pcol(mygrid.XC,mygrid.YC,tmp(:,:,iz));']);
		end
	  else
		eval([varName '=TR;']);
	  end
	  save([input_directory '_' varName file_appends(itr2,:)],varName,'x','y','z')
	  write2netcdf([input_directory varName '_' file_appends(itr2,:) '.nc'],TR,x,y,z,[],upper(varName))
  end
end
