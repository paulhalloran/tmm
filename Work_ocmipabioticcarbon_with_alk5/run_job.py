import glob
import sys
import subprocess
import os
import uuid
import shutil
import time
import datetime

def remove_warning_and_delete_if_zero(file):
	f = open(file,"r+")
	d = f.readlines()
	f.seek(0)
	for i in d:
		if not i.strip().startswith('Warning:'):
			f.write(i)
	f.truncate()
	f.close()
	if os.path.getsize(file) == 0:
		os.remove(file)

##################
# edit this section
##################

your_isca_username = 'ph290'
your_email_address = 'p.halloran@exeter.ac.uk' #edit this so that it has your email address within quotatoin marks
your_project_id_on_isca = 'Undergraduate_Teaching-148395' # if this is not correct change to your research project code e.g. Research_Project-14839ate_Teaching-148395= 1500

run_length_years = 1500

number_of_processors = 16 # recommend 16
job_segment_length_years = 1 # recommend 100, problems likley to occur if this is set to less than 1.
walltime_for_job_segment = '00:00:3:00' # days:hours:minutes:seconds recommend '00:04:00:00'
deltat_clock = 0.0013698630136986 # This is the decimal fraction of a year per model timestep. Recommend 0.0013698630136986 which is 0.5 days in 365 day year
starting_year = 0 # This just determines what year is used in the filename
days_in_year = 365 # This must match what is used in the initial and restart runscripts
supercomputer_queue = 'pq' # this is the queue on the supercomputer that you want to run on. defalt is 'pq' (parallel queue)
number_of_restart_attempts = 3
###
# if you want to pickup from a previous run, specify the below
###
pickup_from_previous_run = False
# If the above is True, you must specify the name used by the job you want to pickup and it's start year, and it must have a pickup.petsc file followed by that job name
jobstring = '2019.05.23_09.40.21_537805' # If pickup_from_previous_run == True, specify the job name e.g. 2018.11.08_20.15.40_979730
start_year = 1200 # this is the year that the previous run ended
itteration_start_number = 6 # which job segment you are starting on (may be different from start year is you started from a non-zero start year). E.g. If you have 100 year job segments and you are starting from year 300, this would be 3
##################
# end
##################

def test_for_text(file,stringToMatch):
	string_found = False
	with open(file, 'r') as file:
		for line in file:
			if stringToMatch in line:
				string_found = True
	return string_found


def test_for_walltime_exceedance(your_isca_username):
	walltime_exceedance = False
	stringToMatch = 'Moab.'
	matchedLine = ''
	#get line
	with open('nohup.out', 'r') as file:
		for line in file:
			if stringToMatch in line:
				matchedLine = line[0:11]
	proc = subprocess.Popen(['showq -c -u '+your_isca_username],stdout=subprocess.PIPE,shell=True)
	output = proc.stdout.read()
	if output[output.find(matchedLine)::].split()[2] == 'CNCLD(271)':
		walltime_exceedance= True
	return walltime_exceedance

try:
	#empty the contents of the nohup.out file from the previous run
	subprocess.call(['cat /dev/null > nohup.out'], shell=True)
except:
	print 'no nohup.out file to move'


print 'run:'
print 'module load PETSc/3.6.3-foss-2016a-Python-2.7.11 before running this'
print 'This script must be running the whole time your job is running. It is therefore recommended that you run it with:'
print 'nohup python2.7 run_job.py &'



timesteps = int((job_segment_length_years * days_in_year) / (round((deltat_clock * days_in_year)*10.0)/10.0))

first_submission_within_pickup_from_previous_run = False

attempts = 0

if not(pickup_from_previous_run):
	print 'New run. Creating initial runscript.'
	# #test for files left over from previous run
	# if os.path.isfile('pickup.petsc'):
	# 	print 'ERROR: remove old pickup.petsc file before running script'
	# 	sys.exit()
	#create a unique, time referenced file name
	now = datetime.datetime.now()
	jobstring = now.strftime('%Y.%m.%d_%H.%M.%S_%f')

	#jobstring = str(uuid.uuid4()) # alternative if you want to have a completely unique filestamp rather than the datetime
	files_created = ['dic_'+jobstring+'.petsc','dic14_'+jobstring+'.petsc','alk_'+jobstring+'.petsc']
	output_directory = 'job_'+jobstring
	if not os.path.exists(output_directory):
		os.makedirs(output_directory)
	shutil.copy('rcak.dat',output_directory+'/rcak.dat')
	shutil.copy('rcab.dat',output_directory+'/rcab.dat')
	shutil.copy('rorgk.dat',output_directory+'/rorgk.dat')
	shutil.copy('rorgb.dat',output_directory+'/rorgb.dat')
	##################
	# setup and run first job segment
	##################
	# These are the text replacements that will be used to convert the template runscript to your runscript
	replace_dict = {}
	replace_dict['walltime=00:00:00:00'] = 'walltime='+walltime_for_job_segment
	replace_dict['$PBS_NODEFILE -np 00'] = '$PBS_NODEFILE -np '+str(number_of_processors)
	replace_dict['-max_steps 00'] = '-max_steps '+str(timesteps)
	# replace_dict['-write_steps 00'] = '-write_steps '+str(timesteps)
	replace_dict['-write_steps 00'] = '-write_steps '+str(timesteps)
	replace_dict['-pickup_out pickup.petsc_unique'] = '-pickup_out pickup.petsc_'+jobstring
	replace_dict['-o dic.petsc,dic14.petsc,alk.petsc'] = '-o dic_'+jobstring+'.petsc,dic14_'+jobstring+'.petsc,alk_'+jobstring+'.petsc'
	replace_dict['-time_file output_time.txt'] = '-time_file output_time_'+jobstring+'.txt'
	replace_dict['> log'] = '> log'+jobstring
	replace_dict['ph290@exeter.ac.uk'] = your_email_address
	replace_dict['Research_Project-148395'] = your_project_id_on_isca
	replace_dict['#PBS -q pq '] = '#PBS -q '+supercomputer_queue+' '
	#perform the text replacements to creat your initial runscript
	fp1 = open('runscript_initial_'+jobstring,"w")
	fp2 = open('runscript_initial_template', 'r')
	data = fp2.read()
	fp2.close()
	for key, value in replace_dict.items():
		data = data.replace(key, value)
	fp1.write(data)
	fp1.close()
	print 'submitting new run: '+jobstring
	subprocess.call(['msub runscript_initial_'+jobstring], shell=True)

##################
# setup and run all subsequent job segments
##################


year = job_segment_length_years
itteration = 1
if pickup_from_previous_run:
	first_submission_within_pickup_from_previous_run = True
	year = start_year + job_segment_length_years
	itteration = itteration_start_number
	if jobstring.split('_')[0] == 'job':
		jobstring = '_'.join(jobstring.split('_')[1:])
	output_directory = 'job_'+jobstring
	if not os.path.exists(output_directory):
		os.makedirs(output_directory)
	files_created = ['dic_'+jobstring+'.petsc','dic14_'+jobstring+'.petsc','alk_'+jobstring+'.petsc']
	try:
		shutil.copy(output_directory+'/'+'pickup.petsc_'+jobstring+'_{0:04}'.format(start_year),'pickup.petsc_'+jobstring)
	except:
		print 'specified pickup file ('+output_directory+'/'+'pickup.petsc_'+jobstring+'_{0:04}'.format(start_year)+') does not exist'
		sys.exit(1)

if not(year <= run_length_years):
	print 'warning, run specified to finish before current year'

while (year <= run_length_years):
	walltime_exceeded = False
	#an error file can be written containing just warnings. In this case, we don't want out program to stop just 'cos it sees an error file. So demove warnings, and if zero size, delete file (careful - could cause issues if you want to see the warnings!)
	initial_error_file = glob.glob('runscript_initial_'+jobstring+'.e*')
	restart_error_file = glob.glob('runscript_restart_'+jobstring+'.e*')
	if len(initial_error_file) <> 0:
		remove_warning_and_delete_if_zero(initial_error_file[0])
	if len(restart_error_file) <> 0:
		remove_warning_and_delete_if_zero(restart_error_file[0])
	# tests to see if error output exists for the job
	test_file1 = (len(glob.glob('runscript_initial_'+jobstring+'.e*')) <> 0)
	test_file2 = (len(glob.glob('runscript_restart_'+jobstring+'.e*')) <> 0)
	#loop restarts a job if final year not yet reached and no error scripts have been produced for this job
	if not(first_submission_within_pickup_from_previous_run):
		continue_loop = True
		while continue_loop == True:
			time.sleep(30) # check if pickup.petsc has been written (i.e. run completed) every 30 seconds
			initial_error_file = glob.glob('runscript_initial_'+jobstring+'.e*')
			restart_error_file = glob.glob('runscript_restart_'+jobstring+'.e*')
			if len(initial_error_file) <> 0:
				remove_warning_and_delete_if_zero(initial_error_file[0])
			if len(restart_error_file) <> 0:
				remove_warning_and_delete_if_zero(restart_error_file[0])
			test_file1 = (len(glob.glob('runscript_initial_'+jobstring+'.e*')) <> 0)
			test_file2 = (len(glob.glob('runscript_restart_'+jobstring+'.e*')) <> 0)
			test_file3 = test_for_text('nohup.out','ERROR:  invalid socket data message size requested')
			if os.path.isfile('pickup.petsc_'+jobstring):
				continue_loop = False
			if (test_file1 | test_file2 | test_file3): # checks to see if error output exists for job, if so, exits loop and therefore script
				continue_loop = False
	test_file1 = (len(glob.glob('runscript_initial_'+jobstring+'.e*')) <> 0)
	test_file2 = (len(glob.glob('runscript_restart_'+jobstring+'.e*')) <> 0)
	if (test_file1 | test_file2): # checks to see if error output exists for job, if so, exits loop and therefore script
		file_to_move = glob.glob('runscript_initial_'+jobstring+'.e*')
		if len(file_to_move) > 0:
			file_to_move = file_to_move[0]
			shutil.move(file_to_move,'old_'+file_to_move)
		file_to_move = glob.glob('runscript_restart_'+jobstring+'.e*')
		print file_to_move
		if len(file_to_move) > 0:
			file_to_move = file_to_move[0]
			print file_to_move
			shutil.move(file_to_move,'old_'+file_to_move)
		walltime_exceeded = test_for_walltime_exceedance(your_isca_username)
		if walltime_exceeded:
			year = year - job_segment_length_years
			attempts += 1
		else: #if walltime issue not hit reset attempts to 0. Iyt will usually be zero, but this will reset to zero
			#in the case that there was preiously a walltime issue, which has now been reolved.
			attempts = 0
		if attempts > number_of_restart_attempts:
			break
	if not(walltime_exceeded):
		for file in files_created:
			try:
				shutil.move(file, output_directory+'/'+file+'{0:04}'.format(starting_year + year))
			except:
				pass
		if os.path.isfile('pickup.petsc_'+jobstring):
			try:
				shutil.move('pickup.petsc_'+jobstring, 'pickup.petsc_'+jobstring+'_{0:04}'.format(starting_year + year))
				shutil.copy('pickup.petsc_'+jobstring+'_{0:04}'.format(starting_year + year),output_directory+'/'+'pickup.petsc_'+jobstring+'_{0:04}'.format(starting_year + year))
			except:
				pass
	#perform the text replacements to create your resubmission runscript
	replace_dict2 = {}
	replace_dict2['walltime=00:00:00:00'] = 'walltime='+walltime_for_job_segment
	replace_dict2['$PBS_NODEFILE -np 00'] = '$PBS_NODEFILE -np '+str(number_of_processors)
	replace_dict2['-max_steps 00'] = '-max_steps '+str(timesteps)
	replace_dict2['-write_steps 00'] = '-write_steps '+str(timesteps)
	replace_dict2['-t0 0.0 -iter0 1'] = '-t0 0.0 -iter0 '+str(itteration)
	replace_dict2['-pickup pickup.petsc_unique'] = '-pickup pickup.petsc_'+jobstring+'_{0:04}'.format(starting_year + year)
	replace_dict2['-o dic.petsc,dic14.petsc,alk.petsc'] = '-o dic_'+jobstring+'.petsc,dic14_'+jobstring+'.petsc,alk_'+jobstring+'.petsc'
	replace_dict2['-time_file output_time.txt'] = '-time_file output_time_'+jobstring+'.txt'
	replace_dict2['-pickup_out pickup.petsc_unique'] = '-pickup_out pickup.petsc_'+jobstring
	replace_dict2['> log'] = '> log'+jobstring
	replace_dict2['ph290@exeter.ac.uk'] = your_email_address
	replace_dict2['Research_Project-148395'] = your_project_id_on_isca
	replace_dict2['#PBS -q pq '] = '#PBS -q '+supercomputer_queue+' '
	fp1 = open('runscript_restart_'+jobstring,"w")
	fp2 = open('runscript_restart_template', 'r')
	data = fp2.read()
	fp2.close()
	for key, value in replace_dict2.items():
		data = data.replace(key, value)
	fp1.write(data)
	fp1.close()
	subprocess.call(['msub runscript_restart_'+jobstring], shell=True)
	first_submission_within_pickup_from_previous_run = False
	year += job_segment_length_years
	itteration += 1


#process files from the last year
if not(walltime_exceeded):
	print 'tidying up after run (if it ran)'
	for file in files_created:
		try:
			shutil.move(file, output_directory+'/'+file+'{0:04}'.format(starting_year + year))
		except:
			pass

	try:
		shutil.move('log'+jobstring, output_directory+'/log'+jobstring)
		shutil.copy('pickup.petsc_'+jobstring+'_{0:04}'.format(starting_year + year), output_directory+'/'+'pickup.petsc_'+jobstring+'_{0:04}'.format(starting_year + year))
	except:
		print 'no log file to tidy up'
