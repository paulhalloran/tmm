CBOP
C !ROUTINE: OCMIP_ABIOTIC_CARBON_INI
#include "OCMIP_ABIOTIC_CARBON_OPTIONS.h"

C !INTERFACE: ==========================================================
      SUBROUTINE OCMIP_ABIOTIC_CARBON_INI(myIter,myTime,
     &                 PTR_DIC,PTR_ALK,PTR_PO4,PTR_SIO2,
     &                 thetaloc,saltloc,pHloc,
     &                 kmt_loc,dz_loc)

C !DESCRIPTION:

C !USES: ===============================================================
      IMPLICIT NONE


!#include "size.h"
!#include "npzd.h"
!#include "coord.h"
#include "bio.h"

C !INPUT PARAMETERS: ===================================================
C  myIter               :: current timestep
C  myTime               :: current time
C  PTR_DIC              :: dissolced inorganic carbon
C  PTR_ALK              :: alkalinity
C  PTR_PO4              :: phosphate
c  PTR_SIO2              :: silicate
      INTEGER myIter
      real*8 myTime
      real*8 PTR_DIC,PTR_ALK,PTR_PO4,PTR_SIO2
      real*8 thetaloc,saltloc,pHloc
      integer kmt_loc
C     Output variables

C !LOCAL VARIABLES: ====================================================
c  rcak = array used in calculating calcite remineralization
c  note these calues affount for remineralisatoni percent
c  and the thickness of the level
c  rcab = array used in calculating bottom calcite remineralization
c  zw = depth to bottom of level from surface (cm)
c  dcaco3 = remineralisation depth of caco3
c  dzt = vertical resolution (cm)
c  max_levels = number of grid points in vertical

      real*8 DIC
      real*8 co2star
      real*8 phlo,phhi
      parameter (phlo=6.d0, phhi=9.d0)
      real*8 rho0, permil, permeg
      parameter (rho0=1024.5d0, permil=1.d0/rho0, permeg=1.d-6)
      real*8 k0,k1,k2,kw,kb,ks,kf,k1p,k2p,k3p,ksi
	  common/carbconst/k1,k2,kw,kb,ks,kf,k1p,k2p,k3p,ksi
      real*8 bt,st,ft
      common/species/bt,st,ft
      real*8 ff,sol,htotal
      INTEGER it,k
      integer nzmax
      real*8 dz_loc(max_levels)
      real*8 zw(max_levels)
      real*8 dzt(max_levels)
      character (len=1000) :: text
      character (len=10) :: word
      integer :: ierr
      real*8 daysec

CEOP

C     Copy surface variables
      DIC=PTR_DIC

	  call carbon_coeffs(thetaloc,saltloc,ff,k0,k1,k2,kb,k1p,k2p,
     &                   k3p,ksi,kw,ks,kf,bt,st,ft,1)

C ---- MIT solver ------------------------------------------
	  pHloc=8.d0
	  do it=1,10 ! iterate to convergence
		call calc_co2_approx(thetaloc,saltloc,DIC,PTR_PO4,PTR_SIO2,
     &             PTR_ALK,k1,k2,k1p,k2p,k3p,ks,kb,kw,ksi,kf,ff,bt,
     &             st,ft,pHloc,co2star)
	  enddo
C ---- MIT solver ------------------------------------------

C ---- Newton solver ------------------------------------------
C      call co2_newton(DIC,PTR_ALK,PTR_PO4,PTR_SIO2,phlo,phhi,
C     &                pHloc,co2star)
C ---- Newton solver ------------------------------------------


!-----------------------------------------------------------------------
!     read grid
!     from mobi1.6/src/mobi_calc.F
!-----------------------------------------------------------------------

C Recompute local grid-dependent data
CSPK copy over data/set constants

!      dzt(:) = 0.d0
!      zw(:) = 0.d0
!      rcak(:) = 0.d0
!      rcab(:) = 0.d0
!      rorgk(:) = 0.d0
!      rorgb(:) = 0.d0

      do k=1,kmt_loc
        dzt(k) = dz_loc(k)
!        zt(k) = z(k)
      enddo

      zw(1) = dz_loc(1)
      do k=2,kmt_loc
        zw(k) = zw(k-1) + dz_loc(k)
      enddo

CCCCC

C!---------------------------------------------------------------------
C!     calculate variables used in calcite remineralization
C!     from uvok_ini.F
C!---------------------------------------------------------------------
C
C      dcaco3 = 6500.d0  ! remineralisation depth of calcite [m]
C
C	  rcak(1) = -(exp(-zw(1)/dcaco3)-1.0)/dzt(1)
C      rcab(1) = 1./dzt(1)
C      do k=2,kmt_loc
C			rcak(k) = -(exp(-zw(k)/dcaco3))/dzt(k)
C     &          + (exp(-zw(k-1)/dcaco3))/dzt(k)
C			rcab(k) = (exp(-zw(k-1)/dcaco3))/dzt(k)
C      enddo


!---------------------------------------------------------------------
!     prescribing variables used in calcite and organic carbon remineralization

!---------------------------------------------------------------------

! rcak is fraction that dissolves at each level assuming an infinite dissolution profile
! rcab is the fraction that would dissolve in each level if that were the bottom of the ocean,
! rorg... are the equivilants for organic carbon remineralisation
! i.e. what remains after what has dissolved in the levels above (1 minus the cumulative sum of the levels above)
! Note that the data in the rcab and rcak files (rcab.dat and rcak.dat) should be a comma separated list in the format 1.00000000 (i.e. 6 decimal places)
! Example rcab: 1.000000,1.000000,1.000000,1.000000,0.100000,0.650000,0.400000,0.400000,0.400000,0.400000,0.400000,0.300000,0.150000,0.050000,0.000000,
! Example rcak: 0.000000,0.000000,0.000000,0.000000,0.350000,0.250000,0.000000,0.000000,0.000000,0.000000,0.100000,0.150000,0.100000,0.050000,0.000000,

      OPEN(UNIT=10, FILE="rcak.dat")
      READ(10,'(15f9.6)') rcak(:)
      ! note the 15 above woudl have to be changed if working with model with more/less depth levels - make this more generic
      CLOSE (10)
!      write(*,*) "PH 0 Debugging"
!      write(*,*) "rcak: ",rcak
! This next bit simply divides the values by the thickness of that depth level
!      do k=1,kmt_loc
!        write(*,*) "rcak(k) before: ",rcak(k)
!        write(*,*) "dzt(k): ",dzt(k)
!        rcak(k) = rcak(k)/dzt(k)
!        write(*,*) "rcak(k) after : ",rcak(k)
!      enddo

      OPEN(UNIT=10, FILE="rcab.dat")
      READ(10,'(15f9.6)') rcab(:)
      CLOSE (10)
!      write(*,*) "PH 1 Debugging"
!      write(*,*) "rcab: ",rcab
!      do k=1,kmt_loc
!        rcab(k) = rcab(k)/dzt(k)
!      enddo

      OPEN(UNIT=10, FILE="rorgk.dat")
      READ(10,'(15f9.6)') rorgk(:)
      CLOSE (10)
!      do k=1,kmt_loc
!        rorgk(k) = rorgk(k)/dzt(k)
!      enddo

      OPEN(UNIT=10, FILE="rorgb.dat")
      READ(10,'(15f9.6)') rorgb(:)
      CLOSE (10)
!      do k=1,kmt_loc
!        rorgb(k) = rorgb(k)/dzt(k)
!      enddo


!      write(*,*) "PH 2 Debugging"
!      write(*,*) "dz_loc: ",dz_loc
!      write(*,*) "zw: ",zw
!      write(*,*) "dzt",dzt
!      write(*,*) "rcak: ",rcak
!      write(*,*) "rcab: ",rcab

      open (10,file="namelist",action="read")
      do
         read (10,"(a)",iostat=ierr) text ! read line into character variable
         if (ierr /= 0) exit
         read (text,*) word ! read first word of line
         if (word == 'prca') then ! found search string at beginning of line
            read (text,*) word,prca
         end if
      end do
      close (10)

      open (10,file="namelist",action="read")
      do
         read (10,"(a)",iostat=ierr) text ! read line into character variable
         if (ierr /= 0) exit
         read (text,*) word ! read first word of line
         if (word == 'prorgc') then ! found search string at beginning of line
            read (text,*) word,prorgc
         end if
      end do
      close (10)


      daysec = 86400.0
      prca = prca / daysec
      prorgc = prorgc / daysec
C acounts for the fact that I think the GDIC etc. value s are change per second
C   Could thise GDIC etc value be per second, so get multiplied by  biogeochem_deltat python2.7 before applied?
C I think this is it - see  /* Convert to discrete tendency */  ierr = VecScale(JDIC,DeltaT);CHKERRQ(ierr);
C in external_forcing_ocmip_abiotic_carbon_landatm.c

	  RETURN
	  END
