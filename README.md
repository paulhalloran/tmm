This repository contains relatively minor modifications to Samar Khatiwala's TMM package for offline ocean biogeochemical simulations:

https://github.com/samarkhatiwala/tmm

I strongly recommend defaulting to the original version for any genral applications.

The modifications add alkalinity to the abiotic OCMIP confituration, then add the ability to prescribe production and dissolution/remineralisation of organic and inorganic carbon.

This instructions are very specific to the University of Exeter system.

logon to isca
in $HOME/TMM/tmm/ make a new Working directory. E.g.: $HOME/TMM/tmm/Work_ocmipabioticcarbon
cd into this
module load MATLAB/2016a
module load PETSc/3.6.3-foss-2016a-Python-2.7.11
copy required files to your new directory:
cp -r $HOME/TMM/tmm/driver/current/* .
alternatively I've copied the files from my set uo to a shared space, so you could get them with:
cp -r /gpfs/ts0/projects/Undergraduate_Teaching-148395/TMM_files/* .
get new files specific to this setup:
wget https://bitbucket.org/paulhalloran/tmm/raw/cb41ddbdd0f36b9dae544dff4f8f81b78868c1c5/Work_ocmipabio
ticcarbon_with_alk5/OCMIP_ABIOTIC_CARBON_OPTIONS.h
wget https://bitbucket.org/paulhalloran/tmm/raw/cb41ddbdd0f36b9dae544dff4f8f81b78868c1c5/Work_ocmipabio
ticcarbon_with_alk5/carbon_chem.F
wget https://bitbucket.org/paulhalloran/tmm/raw/cb41ddbdd0f36b9dae544dff4f8f81b78868c1c5/Work_ocmipabio
ticcarbon_with_alk5/external_forcing_ocmip_abiotic_carbon_landatm.c
wget https://bitbucket.org/paulhalloran/tmm/raw/cb41ddbdd0f36b9dae544dff4f8f81b78868c1c5/Work_ocmipabio
ticcarbon_with_alk5/landsource.F
wget
https://bitbucket.org/paulhalloran/tmm/raw/6ef12cd1d8b584ae02a92891975b400120fb7290/Work_ocmipabiotic
carbon_with_alk5/ocmip_abiotic_carbon_ini.F
wget https://bitbucket.org/paulhalloran/tmm/raw/cb41ddbdd0f36b9dae544dff4f8f81b78868c1c5/Work_ocmipabio
ticcarbon_with_alk5/ocmip_abiotic_carbon_landatm.h
wget https://bitbucket.org/paulhalloran/tmm/raw/6ef12cd1d8b584ae02a92891975b400120fb7290/Work_ocmipa
bioticcarbon_with_alk5/ocmip_abiotic_carbon_model.F
wget https://bitbucket.org/paulhalloran/tmm/raw/68fdf9a11dd89352ad986c23e0d65d0463ebbc10/Work_ocmipa
bioticcarbon_with_alk5/bio.h
wget https://bitbucket.org/paulhalloran/tmm/raw/68fdf9a11dd89352ad986c23e0d65d0463ebbc10/Work_ocmipa
bioticcarbon_with_alk5/rcab.dat
wget https://bitbucket.org/paulhalloran/tmm/raw/68fdf9a11dd89352ad986c23e0d65d0463ebbc10/Work_ocmipa
bioticcarbon_with_alk5/rcak.dat
wget https://bitbucket.org/paulhalloran/tmm/raw/68fdf9a11dd89352ad986c23e0d65d0463ebbc10/Work_ocmipa
bioticcarbon_with_alk5/rorgb.dat
wget https://bitbucket.org/paulhalloran/tmm/raw/68fdf9a11dd89352ad986c23e0d65d0463ebbc10/Work_ocmipa
bioticcarbon_with_alk5/rorgk.dat
wget https://bitbucket.org/paulhalloran/tmm/raw/68fdf9a11dd89352ad986c23e0d65d0463ebbc10/Work_ocmipa
bioticcarbon_with_alk5/namelist
remove the previous Makefile and download the new one
rm -rf Makefile
wget https://bitbucket.org/paulhalloran/tmm/raw/dc43c8b66a105efb0b769873173e873935a261d6/Work_ocmipa
bioticcarbon_with_alk5/Makefile .
Compile the code:
make clean
make tmmocmipabiolandatm
make tmmocmipabiolandatmspinup
Copy the matlab generated files required for running the model
cp $HOME/TMM/tmm/models/current/ocmipabioticcarbon_with_alk/matlab/* .
alternatively, I've copied by files to a shared space, so you can do:
cp /gpfs/ts0/projects/Undergraduate_Teaching-148395/TMM_matlab_files/* .


rcak.dat specifies for each of the model's 15 vertical levels what fraction of the calcium carbonate exported from
the surface dissolves. e.g. if all values are zero (and note they must be in the form 0.000000 i.e. 1 value before
and 6 values after the decimal point) there is no dissolution in the water column (see next file...)
rcab.dat specifies for each of the model's 15 vertical levels, if that is the lowest level in the ocean (i.e. it is
sea-floor below this level at that point in space) what fraction of the calcium carbonate exported from the surface
dissolves. This must be 1 minus what has dissolved in the water column above this point. i.e. for the top level
(the first in the list) this should alway be 1.000000 because if the ocean is only one level deep at that point, all of
the calcium carbonate must dissolve in that level - it can't go any deeper.
rorgk.dat and rorgb.dat are the same as the above two, but for organic carbon, not calcium carbonate
The depths associated with the bottom of each vertical level are:
50m, 120, 220, 360, 550, 790, 1080, 1420, 1810, 2250, 2740, 3280, 3870, 4510, 5200m
The model does not have to be recompiled after changing these values, it can simply be run again.
Some relevant papers:
https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2006GB002803
https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2006GB002907
Resubmission runs (way to do long runs)
log on to supercomputer
navigate to the directory containing your model setup
If this is the first time you are doing this, download the resubmission script by typing:
wget https://bitbucket.org/paulhalloran/tmm/raw/a29cad720bd0ea6bcf82da55b98e336d5c5db210/Work_ocmipa
bioticcarbon_with_alk5/run_job.py .
wget https://bitbucket.org/paulhalloran/tmm/raw/f9ad7a1fb26a85f9f7e64056d5927c53c675fee1/Work_ocmipabi
oticcarbon_with_alk5/runscript_initial_template .
wget https://bitbucket.org/paulhalloran/tmm/raw/f9ad7a1fb26a85f9f7e64056d5927c53c675fee1/Work_ocmipabi
oticcarbon_with_alk5/runscript_restart_template .
edit the file run_job.py - you will probably inly want to change the lines "your_email_address = 'p.halloran@exete
r.ac.uk'" and "run_length_years = 5000"
Load the module required to run the model by typing:
module load PETSc/3.6.3-foss-2016a-Python-2.7.11
Make any changes to the model setup that you require then run the script to keep resubmitting the model by
typing:
nohup python2.7 run_job.py &
press enter
Loading output from resubmission runs:
log on to supercomputer
navigate to the directory containing your model setup
If doing this for the 1st time, get hold of the upload script:
wget https://bitbucket.org/paulhalloran/tmm/raw/a29cad720bd0ea6bcf82da55b98e336d5c5db210/Work_ocmipa
bioticcarbon_with_alk5/load_output_with_restarts.m .
When runs are undertaken in this way the output files are slightly differently organised. To porocess the output
files to produce netcdf files first edit the script:
load_output_with_restarts.m
so that the 1st line is pointing to the directory containing the output (if you just list the contents of your directory it
should be obvious which one this is because of the timestamp n the directory name)
then load matlab
module load MATLAB
then open matlab
matlab
then run the processing script:
run load_output_with_restarts.m
The netcdf files that the script outputs should now be in your timestamped output directory

also need to get a copy of andf run:
make_input_files_for_ocmip_abiotic_carbon_mode_test_diss.m
and
make_input_files_for_ocmip_abiotic_carbon_model_export_dis.m

Calcite dissolution:

Changing the prescribed orgC or CaCO3 export, or the dissolution profile:

In
/gpfs/ts0/home/ph290/TMM/tmm/tmm_matrices_data/MITgcm_2.8deg/GCM/

There are files (e.g.)

ProductivityForcing_sat
caco3_diss_shallow.mat

which are used to produced the binary files which drive the model within the script within Work_ocmipabioticcarbon:
make_input_files_for_ocmip_abiotic_carbon_model_export_dis.m

Edit the file using the information below, to point to your input Matlab files, then in Matlab to produce the forcing files to be used.

Prod_x  (were x is the month number) organic carbon export from file specified in line prodForcingFile=fullfile(prodDataPath,'ProductivityForcing_sat'); (variable Prod_sat)
CalProd_x (were x is the month number) CaCO3 export  from file specified in line prodForcingFile=fullfile(prodDataPath,'ProductivityForcing_sat'); (variable Prod_cal)

Cd_x (were x is the month number) CaCO3 dissolution fraction if not at the sea floor from file specified in line load(fullfile(prodDataPath,'caco3_diss_shallow.mat'),'cal_diss', 'calbot_diss') (variable cal_diss)
Cbd_x (were x is the month number) CaCO3 dissolution fraction if at the sea floor
 from file specified in line load(fullfile(prodDataPath,'caco3_diss_shallow.mat'),'cal_diss', 'calbot_diss’), (variable calbot_diss)

