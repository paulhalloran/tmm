import glob
import sys
import subprocess
import os
import uuid
import shutil
import time
import datetime

def remove_warning_and_delete_if_zero(file):
	f = open(file,"r+")
	d = f.readlines()
	f.seek(0)
	for i in d:
		if not i.strip().startswith('Warning:'):
			f.write(i)
	f.truncate()
	f.close()
	if os.path.getsize(file) == 0:
		os.remove(file)

##################
# edit this section
##################

run_length_years = 10

number_of_processors = 96 # recommend 96
job_segment_length_years = 1 # recommend 10
walltime_for_job_segment = '00:00:05:00' # days:hours:minutes:seconds recommend '00:00:30:00'
deltat_clock = 0.0013698630136986 # This is the decimal fraction of a year per model timestep. Recommend 0.0013698630136986 which is 0.5 days in 365 day year
starting_year = 0 # This just determines what year is used in teh filen
days_in_year = 365 # This must match what is used in the initial and restart runscripts
#List the output files produced by your model setup
files_created = ['po4avg.petsc','dopavg.petsc','oxyavg.petsc','phyavg.petsc','zooavg.petsc','detavg.petsc','no3avg.petsc','dicavg.petsc','alkavg.petsc','fbgc1.petsc','fbgc2.petsc','fbgc3.petsc','fbgc4.petsc','fbgc5.petsc','fbgc6.petsc','fbgc7.petsc','po4.petsc','dop.petsc','oxy.petsc','phy.petsc','zoo.petsc','det.petsc','no3.petsc','dic.petsc','alk.petsc']
###
# if you want to pickup from a previous run, specify the below
###
pickup_from_previous_run = False
# If the above is True, you must specify the name used by the job you want to pickup and it's start year, and it must have a pickup.petsc file followed by that job name
jobstring = '2017.11.23_12.44.49_170410' # If pickup_from_previous_run == True, specify the job name
start_year = 1 # if 
itteration_start_number = 1 # which job segment you are starting on (may be different from start year is you started from a non-zero start year)
##################
# end
##################

print 'run module load PETSc before running this'
print 'This script must be running the whole time your job is running. It is therefore recommended that you run it after initialising a *screen* session'

timesteps = int((job_segment_length_years * days_in_year) / (round((deltat_clock * days_in_year)*10.0)/10.0))

if not(pickup_from_previous_run):

	#test for files left over from previous run
	if os.path.isfile('pickup.petsc'):
		print 'ERROR: remove old pickup.petsc file before running script'
		sys.exit()

	#create a unique, time referenced file name
	now = datetime.datetime.now()
	jobstring = now.strftime('%Y.%m.%d_%H.%M.%S_%f')
	#jobstring = str(uuid.uuid4()) # alternative if you want to have a completely unique filestamp rather than the datetime
	output_directory = 'job_'+jobstring
	if not os.path.exists(output_directory):
		os.makedirs(output_directory)

	##################
	# setup and run first job segment
	##################

	# These are the text replacements that will be used to convert the template runscript to your runscript
	replace_dict = {}
	replace_dict['walltime=00:00:00:00'] = 'walltime='+walltime_for_job_segment
	replace_dict['$PBS_NODEFILE -np 96'] = '$PBS_NODEFILE -np '+str(number_of_processors)
	replace_dict['-max_steps 730'] = '-max_steps '+str(timesteps)
	replace_dict['-write_steps 730'] = '-write_steps '+str(timesteps)

	#perform the text replacements to creat your initial runscript
	fp1 = open('runscript_initial_'+jobstring,"w")
	fp2 = open('runscript_initial_template', 'r')
	data = fp2.read()
	fp2.close()
	for key, value in replace_dict.items():
		data = data.replace(key, value)
	fp1.write(data)
	fp1.close()

	subprocess.call(['msub runscript_initial_'+jobstring], shell=True)

##################
# setup and run all subsequent job segments
##################

year = job_segment_length_years
itteration = 1
if pickup_from_previous_run:
	year = start_year
	output_directory = 'job_'+jobstring
	itteration = itteration_start_number
	output_directory = 'job_'+jobstring
	if not os.path.exists(output_directory):
		os.makedirs(output_directory)


while (year <= run_length_years):

	replace_dict2 = {}
	replace_dict2['walltime=00:00:00:00'] = 'walltime='+walltime_for_job_segment
	replace_dict2['$PBS_NODEFILE -np 96'] = '$PBS_NODEFILE -np '+str(number_of_processors)
	replace_dict2['-max_steps 730'] = '-max_steps '+str(timesteps)
	replace_dict2['-write_steps 730'] = '-write_steps '+str(timesteps)
	replace_dict2['-t0 0.0 -iter0 1'] = '-t0 0.0 -iter0 '+str(itteration)
	replace_dict2['-pickup pickup.petsc'] = '-pickup pickup.petsc'+'{0:04}'.format(starting_year + year)

	#an error file can be written containing just warnings. In this case, we don't want out program to stop just 'cos it sees an error file. So demove warnings, and if zero size, delete file (careful - could cause issues if you want to see the warnings!)
	initial_error_file = glob.glob('runscript_initial_'+jobstring+'.e*')
	restart_error_file = glob.glob('runscript_restart_'+jobstring+'.e*')

	if len(initial_error_file) <> 0:
		remove_warning_and_delete_if_zero(initial_error_file[0])


	if len(restart_error_file) <> 0:
		remove_warning_and_delete_if_zero(restart_error_file[0])


	# tests to see if error output exists for the job
	test_file1 = (len(glob.glob('runscript_initial_'+jobstring+'.e*')) <> 0)
	test_file2 = (len(glob.glob('runscript_restart_'+jobstring+'.e*')) <> 0)

	#loop restarts a job if final year not yet reached and no error scripts have been produced for this job
	while True:
		time.sleep(20) # check if pickup.petsc has been written (i.e. run completed) every 20 seconds

		initial_error_file = glob.glob('runscript_initial_'+jobstring+'.e*')
		restart_error_file = glob.glob('runscript_restart_'+jobstring+'.e*')

		if len(initial_error_file) <> 0:
			remove_warning_and_delete_if_zero(initial_error_file[0])


		if len(restart_error_file) <> 0:
			remove_warning_and_delete_if_zero(restart_error_file[0])

		test_file1 = (len(glob.glob('runscript_initial_'+jobstring+'.e*')) <> 0)
		test_file2 = (len(glob.glob('runscript_restart_'+jobstring+'.e*')) <> 0)
		if os.path.isfile('pickup.petsc'):
			break
		if (test_file1 | test_file2): # checks to see if error output exists for job, if so, exits loop
			break

	if (test_file1 | test_file2): # checks to see if error output exists for job, if so, exits loop and therefore script
		break   

	for file in files_created:
		shutil.move(file, output_directory+'/'+file+'{0:04}'.format(starting_year + year))

	shutil.move('pickup.petsc', 'pickup.petsc'+'{0:04}'.format(starting_year + year))
	shutil.copy('pickup.petsc'+'{0:04}'.format(starting_year + year),output_directory+'/'+'pickup.petsc'+'{0:04}'.format(starting_year + year))	

	#perform the text replacements to create your resubmission runscript
	fp1 = open('runscript_restart_'+jobstring,"w")
	fp2 = open('runscript_restart_template', 'r')
	data = fp2.read()
	fp2.close()
	for key, value in replace_dict2.items():
		data = data.replace(key, value)


	fp1.write(data)
	fp1.close()

	subprocess.call(['msub runscript_restart_'+jobstring], shell=True)
	year += job_segment_length_years
	itteration += 1


#process files form the last year
for file in files_created:
   shutil.move(file, output_directory+'/'+file+'{0:04}'.format(starting_year + year))


shutil.copy('pickup.petsc', output_directory+'/'+'pickup.petsc'+'{0:04}'.format(starting_year + year))


