!====================== include file "bio.h" =========================

!   variables for biological component of model

!   max_levels = number of grid points in the vertical direction
!              (calculated points are from 1 through max_levels)
!   dcaco3   = remineralisation depth of calcite [cm]
!   rcak     = array used in calculating calcite remineralization
!   rcab     = array used in calculating bottom calcite remineralization
!   rorgk     = array used in calculating organic carbon remineralization
!   rorgb     = array used in calculating bottom organic carbon remineralization

      integer max_levels

      parameter (max_levels= 15)

      real*8 dcaco3, rcak, rcab, rorgk, rorgb, prca, prorgc

      common /bio/ dcaco3, rcak(max_levels), rcab(max_levels),
     &             rorgk(max_levels), rorgb(max_levels), prca, prorgc
