Note, this is just a local setup and modifications of Samar Khatiwala's TMM package for offline ocean biogeochemical simulations:

https://github.com/samarkhatiwala/tmm

Please default to the original version unless you specifically require the Exeter setup etc.